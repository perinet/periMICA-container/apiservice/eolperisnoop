/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package eolperisnoop

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/filestorage"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
)

func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/info", Method: httpserver.GET, Role: rbac.NONE, Call: Eolperisnoop_Info_Get},
		{Url: "/eolperisnoop/config", Method: httpserver.GET, Role: rbac.NONE, Call: Eolperisnoop_Config_Get},
		{Url: "/eolperisnoop/config", Method: httpserver.PATCH, Role: rbac.NONE, Call: Eolperisnoop_Config_Set},
		{Url: "/eolperisnoop/execute", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: Eolperisnoop_Execute_Set},
		{Url: "/eolperisnoop/execute-golden-tests", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: Eolperisnoop_Execute_Golden_Tests_Set},
		{Url: "/eolperisnoop/log-files", Method: httpserver.GET, Role: rbac.ADMIN, Call: Eolperisnoop_Log_Get},
		{Url: "/eolperisnoop/fetch-log-output", Method: httpserver.GET, Role: rbac.ADMIN, Call: Eolperisnoop_fetch_log_output},
		{Url: "/eolperisnoop/retry", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: Eolperisnoop_Retry_Set},
		{Url: "/eolperisnoop/config-file", Method: httpserver.GET, Role: rbac.NONE, Call: Eolperisnoop_Config_File_Get},
		{Url: "/eolperisnoop/config-file", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: Eolperisnoop_Config_File_Set},
		{Url: "/eolperisnoop/firmware/coprocessor", Method: httpserver.PUT, Role: rbac.ADMIN, Call: Eolperisnoop_Coprocessor_Firmware_Set},
		{Url: "/eolperisnoop/firmware/perisnoop", Method: httpserver.PUT, Role: rbac.ADMIN, Call: Eolperisnoop_periSNOOP_Firmware_Set},
		{Url: "/eolperisnoop/status", Method: httpserver.PUT, Role: rbac.INTERNAL, Call: Eolperisnoop_periSNOOP_Status_Set},
		{Url: "/eolperisnoop/status", Method: httpserver.DELETE, Role: rbac.INTERNAL, Call: Eolperisnoop_periSNOOP_Status_Reset},
	}
}

var Status = struct {
	RUNNING     string
	STOP        string
	ERROR       string
}{
	"RUNNING",
	"STOP",
	"ERROR",
}


type Eolperisnoop_info struct {
	ApiVersion string                `json:"api_version"`
	Config     Eolperisnoop_config   `json:"config"`
	Status     string                `json:"status"`  
}

type Eolperisnoop_config struct {
	TestName string `json:"test_name"`
}

type Eolperisnoop_log struct {
	LogFileName string `json:"log_file_name"`
}

const (
	API_VERSION          = "20"
	SYSTEMD_SERVICE      = "eolperisnoop.apiservice"
	// eolperisnoop_CONFIG_FILE   = "/var/lib/eolperisnoop_service/eolperisnoop.json"
	eolperisnoop_CONFIG_FILE   = "eolperisnoop.json"
	TESTS_PATH     			   = "/home/robot/"
	LATEST_TEST_PATH_FILE	   = "latest_test.txt"
	CONFIG_TEST_FILE		   = "config.py"
	LOG_TEST_PATH			   = "/var/lib/api_service/log_output.txt"
)

// variables
var (
	Logger log.Logger = *log.Default()

	config = Eolperisnoop_config{
		TestName:       "",
	}

	nodeInfo node.NodeInfo
	status_cache = Status.STOP
)

func init() {

	err := filestorage.LoadObject(eolperisnoop_CONFIG_FILE, &config)
	if err != nil {
		log.Println("error reading security configuration file", err)
	}

	data := intHttp.Get(node.NodeInfoGet, nil)
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		Logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}
}

func Eolperisnoop_Info_Get(p periHttp.PeriHttp) {
	var http_status int = http.StatusOK
	var info = Eolperisnoop_info{
		ApiVersion: API_VERSION,
		Status: status_cache,
	}
	res, err := json.Marshal(info)
	if err != nil {
		log.Println(" Eolperisnoop_Info_Get ", err.Error())
		http_status = http.StatusInternalServerError
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	p.JsonResponse(http_status, res)

}

func Eolperisnoop_Config_Get(p periHttp.PeriHttp) {
	var http_status int = http.StatusOK
	res, err := json.Marshal(config)
	if err != nil {
		log.Println(" Eolperisnoop_Config_Get ", err.Error())
		http_status = http.StatusInternalServerError
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	p.JsonResponse(http_status, res)
}

func Eolperisnoop_Config_Set(p periHttp.PeriHttp) {

	payload, _ := p.ReadBody()
	tmp_config := config

	err := json.Unmarshal(payload, &tmp_config)
	if err != nil {
		log.Println(" Eolperisnoop_Config_Set: ", err.Error())
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	if config.TestName != "" {
		// save the config and send response
		config = tmp_config
		log.Println("setting config: ", config)
		filestorage.StoreObject(eolperisnoop_CONFIG_FILE, config)
		p.EmptyResponse(http.StatusOK)

	} else {
		// setup the eolperisnoop
		app_name := tmp_config.TestName
		log.Println(" test_name= ", app_name)

		if app_name == "" {
			p.EmptyResponse(http.StatusInternalServerError)
			return
		}
	}
}

func Eolperisnoop_Execute_Set(p periHttp.PeriHttp) {
	
	// update status
	status_cache = Status.RUNNING

	// trigger the main.py that is the starting point of the tests
	log_output := shellhelper.ShellCall("cd " + TESTS_PATH + "; python3 main.py periCORE")

	currentTime := time.Now().Format("2006-01-02_150405")

	dutName := shellhelper.ShellCall("grep -oP '\\${SERNM} = \\K[^}]+' " + TESTS_PATH + "/output.xml | sed 's/<\\/msg>//'")
	if len(dutName) == 0 {
		dutName = "dut-notfound"
	}

	dutName = strings.ReplaceAll(dutName, " ", "")
	dutName = strings.ReplaceAll(dutName, "\n", "")
	
	shellhelper.ShellCall("mv " + TESTS_PATH + "log.html " + TESTS_PATH + "/log/log-" + dutName + "-" + currentTime + ".html")
	shellhelper.ShellCall("mv " + TESTS_PATH + "output.xml " + TESTS_PATH + "log/log-" + dutName + "-" + currentTime + ".xml")

	var log_output_file Eolperisnoop_log
	log_output_file.LogFileName = "log-"  + dutName + "-" + currentTime + ".html"

	// store lastest log
	filestorage.StoreData(LOG_TEST_PATH, []byte(log_output))

	status_cache = Status.STOP

	// return the log file name
	p.JsonResponse(http.StatusOK, log_output)
}

func Eolperisnoop_Log_Get(p periHttp.PeriHttp) {
	
	shellhelper.ShellCall("zip -r " + TESTS_PATH + "/log.zip " + TESTS_PATH + "/log/*")
	log_file_output, err := os.ReadFile(TESTS_PATH + "/log.zip")
	if err != nil { 
		log.Println(" Eolperisnoop_Log_Get: ", err.Error())
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	p.BinaryResponse(http.StatusOK, log_file_output)
}


func Eolperisnoop_Execute_Golden_Tests_Set(p periHttp.PeriHttp) {

	// update status
	status_cache = Status.RUNNING

	log_output := shellhelper.ShellCall("cd " + TESTS_PATH + "; python3 main.py periSNOOP")

	currentTime := time.Now().Format("2006-01-02_150405")

	dutName := shellhelper.ShellCall("grep -oP '\\${SERNM} = \\K[^}]+' " + TESTS_PATH + "output.xml | sed 's/<\\/msg>//'")
	if len(dutName) == 0 {
		dutName = "dut-notfound"
	}

	dutName = strings.ReplaceAll(dutName, " ", "")
	dutName = strings.ReplaceAll(dutName, "\n", "")

	shellhelper.ShellCall("mv " + TESTS_PATH + "log.html " + TESTS_PATH + "/log/log-gold-example-" + dutName + "-" + currentTime + ".html")
	shellhelper.ShellCall("mv " + TESTS_PATH + "output.xml " + TESTS_PATH + "/log/log-gold-example-" + dutName + "-" + currentTime + ".xml")

	var log_output_file Eolperisnoop_log
	log_output_file.LogFileName = "log-" + dutName + "-" + currentTime + ".html"

	// store lastest log
	filestorage.StoreData(LOG_TEST_PATH, []byte(log_output))

	// update status
	status_cache = Status.STOP

	// return the log file name
	p.JsonResponse(http.StatusOK, log_output)
}

func Eolperisnoop_Retry_Set(p periHttp.PeriHttp) {
	
	status_cache = Status.RUNNING
	log_output := ""
	sernm := ""

	// open file, if there is no serial number or the file does not exists call tests for periCORE
	content, err := os.ReadFile(TESTS_PATH + LATEST_TEST_PATH_FILE)
	if err != nil {
		log.Println("Error reading file:", err)
	} else {
		sernm = string(content)
	}

	log.Println("sernm:", sernm)

	if (sernm == "") {
		log_output = shellhelper.ShellCall("cd " + TESTS_PATH + "; python3 main.py periCORE")
	} else {
		log_output = shellhelper.ShellCall("cd " + TESTS_PATH + "; python3 main.py periSNOOP")
	}
	
	currentTime := time.Now().Format("2006-01-02_150405")

	dutName := shellhelper.ShellCall("grep -oP '\\${SERNM} = \\K[^}]+' "+ TESTS_PATH + "output.xml | sed 's/<\\/msg>//'")
	if len(dutName) == 0 {
		dutName = "dut-notfound"
	}

	dutName = strings.ReplaceAll(dutName, " ", "")
	dutName = strings.ReplaceAll(dutName, "\n", "")
	
	shellhelper.ShellCall("mv " + TESTS_PATH + "/log.html " +  TESTS_PATH + "log/log-retry-" + dutName + "-" + currentTime + ".html")
	shellhelper.ShellCall("mv "+ TESTS_PATH + "/output.xml " + TESTS_PATH + "log/log-retry-" + dutName + "-" + currentTime + ".xml")

	var log_output_file Eolperisnoop_log
	log_output_file.LogFileName = "log-retry-"  + dutName + "-" + currentTime + ".html"

	// store lastest log
	filestorage.StoreData(LOG_TEST_PATH, []byte(log_output))

	status_cache = Status.STOP

	// return the log file name
	p.JsonResponse(http.StatusOK, log_output)
}

func Eolperisnoop_Config_File_Get(p periHttp.PeriHttp) {
	
	config_file_output, err := os.ReadFile(TESTS_PATH + CONFIG_TEST_FILE)
	
	if err != nil { 
		log.Println(" Eolperisnoop_Config_File_Get: ", err.Error())
		p.EmptyResponse(http.StatusBadRequest)
	}

	p.BinaryResponse(http.StatusOK, config_file_output)
}

func Eolperisnoop_Config_File_Set(p periHttp.PeriHttp) {

	payload, _ := p.ReadBody()

	file, err := os.Create(TESTS_PATH + CONFIG_TEST_FILE)
    if err != nil {
        log.Println("Error creating config file: ", err)
		p.EmptyResponse(http.StatusInternalServerError)
    }
    defer file.Close()

    _, err = file.WriteString(string(payload))
    if err != nil {
        log.Println("Error writing to config file: ", err)
        p.EmptyResponse(http.StatusInternalServerError)
    }

	p.EmptyResponse(http.StatusNoContent)
}


func Eolperisnoop_Coprocessor_Firmware_Set(p periHttp.PeriHttp) {

	config_file, err := os.ReadFile(TESTS_PATH + CONFIG_TEST_FILE)
	
	if err != nil { 
		log.Println(" Eolperisnoop_Config_File_Get: ", err.Error())
		p.EmptyResponse(http.StatusBadRequest)
	}

	coprocessorRegex := regexp.MustCompile(`COPROCESSOR_FIRMWARE_IMAGE\s*=\s*"([^"]+)"`)
	coprocessorMatches := coprocessorRegex.FindStringSubmatch(string(config_file))
	coprocessorFirmwareImageFile := coprocessorMatches[1]

	payload, _ := p.ReadBody()	

	if (len(payload) > 0) {
		file, err := os.Create(TESTS_PATH + coprocessorFirmwareImageFile)
		if err != nil {
			log.Println("Error coprocessor file: ", err)
			p.EmptyResponse(http.StatusInternalServerError)
		}
		
		defer file.Close()

		_, err = file.Write(payload)
		if err != nil {
			log.Println("Error writing to coprocessor file: ", err)
			p.EmptyResponse(http.StatusInternalServerError)
		}

		p.EmptyResponse(http.StatusNoContent)
	} else {
		p.EmptyResponse(http.StatusInternalServerError)
	}
}

func Eolperisnoop_periSNOOP_Firmware_Set(p periHttp.PeriHttp) {
	config_file, err := os.ReadFile(TESTS_PATH + CONFIG_TEST_FILE)
	
	if err != nil { 
		log.Println(" Eolperisnoop_Config_File_Get: ", err.Error())
		p.EmptyResponse(http.StatusBadRequest)
	}

	productionRegex := regexp.MustCompile(`PRODUCTION_FIRMWARE\s*=\s*"([^"]+)"`)
	productionMatches := productionRegex.FindStringSubmatch(string(config_file))
	productionFirmwareImageFile := productionMatches[1]

	payload, _ := p.ReadBody()
	
	if (len(payload) > 0) {
		
		file, err := os.Create(TESTS_PATH + productionFirmwareImageFile)
		if err != nil {
			log.Println("Error periSNOOP production firmware: ", err)
			p.EmptyResponse(http.StatusInternalServerError)
		}
		
		defer file.Close()

		_, err = file.Write(payload)
		if err != nil {
			log.Println("Error periSNOOP production firmware: ", err)
			p.EmptyResponse(http.StatusInternalServerError)
		}

		p.EmptyResponse(http.StatusNoContent)
	} else {
		p.EmptyResponse(http.StatusInternalServerError)
	}
}

func Eolperisnoop_periSNOOP_Status_Set(p periHttp.PeriHttp) {

	data, _ := p.ReadBody()
	status := string(data)

	// update status_cache
	status_cache = status

	p.EmptyResponse(http.StatusNoContent)
}

func Eolperisnoop_periSNOOP_Status_Reset(p periHttp.PeriHttp) {
	// update status_cache - stop
	status_cache = Status.STOP
	p.EmptyResponse(http.StatusNoContent)
}

func Eolperisnoop_fetch_log_output(p periHttp.PeriHttp) {

	var log_output []byte
	err := filestorage.LoadData(LOG_TEST_PATH, &log_output)

	if err != nil {
		log.Println("error reading log output file", err)
		res := json.RawMessage(`{"error": "` + err.Error() + `"}`)

		p.JsonResponse(http.StatusInternalServerError, res)
	}

	p.JsonResponse(http.StatusOK, string(log_output))
}