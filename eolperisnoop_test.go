/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package eolperisnoop

import (
	"errors"
	"testing"

	"gotest.tools/v3/assert"

	// "encoding/json"
	// "log"
	// "net/http"
	// "io"
	// "log"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	// "gitlab.com/perinet/generic/lib/utils/intHttp"
	// // "gitlab.com/perinet/generic/lib/utils/filestorage"
	// "gitlab.com/perinet/periMICA-container/apiservice/security"
	// "gitlab.com/perinet/periMICA-container/apiservice/node"
	// "gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	// "gitlab.com/perinet/generic/apiservice/staticfiles"
)

func TestPaths(t *testing.T) {
	paths := PathsGet()
	for _, path := range paths {
		switch path.Url {
		case "/info":
			assert.Assert(t, path.Method == httpserver.GET)
			assert.Assert(t, path.Role == rbac.NONE)
			assert.Assert(t, path.Call != nil)
			return;

		default:
			err := errors.New("Not supported API path: " + path.Url)
			assert.NilError(t, err)
		}
	}
}

func TestMain(t *testing.T) {

	// httpserver.AddPaths(security.PathsGet())
	// // httpserver.AddPaths(lifecycle.PathsGet())
	// httpserver.AddPaths(node.PathsGet())
	// httpserver.AddPaths(staticfiles.PathsGet())
	// httpserver.AddPaths(PathsGet())

	// staticfiles.Set_files_path("./www")
	// // staticfiles.Set_expired_path("./www/expired")

	// intHttp.Patch(security.Set_webserver_cert_path, []byte("/etc/webserver/"))

	// httpserver.SetCertificates(httpserver.Certificates{
	// 	CaCert:      intHttp.VarsGet(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
	// 	HostCert:    intHttp.VarsGet(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
	// 	HostCertKey: intHttp.VarsGet(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	// })

	// httpserver.ListenAndServe("[::]:50443")
}
